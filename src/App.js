import React, { useRef, useState } from "react";
import "./App.css";
import JobLists from "./Components/JobsList";
import JobType from "./Components/JobType";
import Button from "./Components/UI/Button";
import UserInfo from "./Components/UserInfo";
import UsersList from "./Components/UsersList";
import WorkDetail from "./Components/WorkDetail";

const App = () => {
  const [isNameValid, setIsNameValid] = useState(true);
  const [isSNameValid, setIsSNameValid] = useState(true);
  const [isPhoneNumberValid, setIsPhoneNumberValid] = useState(true);
  const [isEmailValid, setIsEmailValid] = useState(true);
  const [isBirthDateValid, setIsBirthDateValid] = useState(true);
  const [isCompanyValid, setIsCompanyValid] = useState(true);
  const [isJobTypeValid, setIsJobTypeValid] = useState(true);
  const [isExperienceValid, setIsExperienceValid] = useState(true);

  const nameInputRef = useRef();
  const sNameInputRef = useRef();
  const phoneNumberRef = useRef();
  const emailInputRef = useRef();
  const birthDateInputRef = useRef();
  const companyNameRef = useRef();
  const jobTypeRef = useRef();
  const experienceRef = useRef();

  const formIsValid =
    isNameValid &&
    isSNameValid &&
    isPhoneNumberValid &&
    isEmailValid &&
    isBirthDateValid &&
    isCompanyValid &&
    isJobTypeValid &&
    isExperienceValid;

  const setAllValidityTrue = () => {
    setIsNameValid(true);
    setIsSNameValid(true);
    setIsPhoneNumberValid(true);
    setIsEmailValid(true);
    setIsBirthDateValid(true);
    setIsCompanyValid(true);
    setIsJobTypeValid(true);
    setIsExperienceValid(true);
  };

  const clearInputsHandler = () => {
    nameInputRef.current.value = "";
    sNameInputRef.current.value = "";
    phoneNumberRef.current.value = "";
    emailInputRef.current.value = "";
    birthDateInputRef.current.value = "";
    companyNameRef.current.value = "";
    jobTypeRef.current.value = "";
    experienceRef.current.value = "";
  };

  const submitFormHandler = (event) => {
    event.preventDefault();
    setAllValidityTrue();
    if (nameInputRef.current.value.trim().length < 1) {
      setIsNameValid(false);
    }
    console.log(nameInputRef.current.value);
    if (sNameInputRef.current.value.trim().length < 1) {
      setIsSNameValid(false);
    }
    if (phoneNumberRef.current.value.trim().length < 6) {
      setIsPhoneNumberValid(false);
    }
    if (!emailInputRef.current.value.trim().includes("@")) {
      setIsEmailValid(false);
    }
    if (birthDateInputRef.current.value.trim().length < 1) {
      setIsBirthDateValid(false);
    }
    if (companyNameRef.current.value.trim().length < 1) {
      setIsCompanyValid(false);
    }
    if (
      jobTypeRef.current.value === "DEFAULT" ||
      jobTypeRef.current.value === ""
    ) {
      setIsJobTypeValid(false);
    }
    if (
      experienceRef.current.value === "DEFAULT" ||
      experienceRef.current.value === ""
    ) {
      setIsExperienceValid(false);
    }
    formIsValid && clearInputsHandler();
    console.log(
      nameInputRef.current.value,
      sNameInputRef.current.value,
      phoneNumberRef.current.value,
      emailInputRef.current.value,
      birthDateInputRef.current.value
    );
  };

  const validity = {
    isNameValid,
    isSNameValid,
    isPhoneNumberValid,
    isEmailValid,
    isBirthDateValid,
    isJobTypeValid,
    isCompanyValid,
    isExperienceValid,
  };

  return (
    <div className="app">
      <div className="container">
        <div className="input-container">
          <UserInfo
            nameInputRef={nameInputRef}
            sNameInputRef={sNameInputRef}
            phoneNumberRef={phoneNumberRef}
            emailInputRef={emailInputRef}
            birthDateInputRef={birthDateInputRef}
            validity={validity}
          />
          <div className="inner">
            <WorkDetail
              companyNameRef={companyNameRef}
              jobTypeRef={jobTypeRef}
              experienceRef={experienceRef}
              validity={validity}
            />
            <JobType />
          </div>
        </div>
        <div className="buttons">
          <Button onClick={clearInputsHandler} type="button" clear>
            Clear
          </Button>
          <Button onClick={submitFormHandler} type="submit">
            Submit
          </Button>
        </div>
      </div>
      <div className="container">
        <UsersList />
      </div>
      <div className="container jobs">
        <JobLists />
      </div>
    </div>
  );
};
export default App;
