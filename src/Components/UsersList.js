import React from "react";

import classes from "./UsersList.module.css";
import deleteBtn from "../assets/delete.svg";

const UsersList = () => {
  return (
    <div className={classes.users}>
      <div className={classes.title}>Users</div>
      <div className={classes.line}></div>
      <div className={classes.names}>
        <div className={classes.item}>№</div>
        <div className={classes.item}>Full name</div>
        <div className={classes.item}>Date of Birth</div>
        <div className={classes.item}>Phone</div>
        <div className={classes.item}>Email</div>
        <div className={classes.item}>Company Name</div>
        <div className={classes.item}>Selected Job</div>
        <div className={classes.item}>Experience</div>
        <div className={classes.item}></div>
      </div>
      <div className={classes.data}>
        <div className={classes.item}>1</div>
        <div className={classes.item}>Full name</div>
        <div className={classes.item}>Date of Birth</div>
        <div className={classes.item}>Phone</div>
        <div className={classes.item}>Email</div>
        <div className={classes.item}>Company Name</div>
        <div className={classes.item}>Selected Job</div>
        <div className={classes.item}>Experience</div>
        <div className={classes.item}>
          <img className={classes["v-open"]} src={deleteBtn} alt="open" />
        </div>
      </div>
    </div>
  );
};

export default UsersList;
