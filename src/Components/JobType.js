import { useRef, useState } from "react";
import Button from "./UI/Button";
import Card from "./UI/Card";
import Input from "./UI/Input";
import classes from "./JobType.module.css";

const JobType = () => {
  const [jobIsValid, setJobIsValid] = useState(true);
  const jobTypeInputRef = useRef();
  const clearHandler = () => {
    jobTypeInputRef.current.value = "";
  };
  const addJobTypeHandler = async () => {
    setJobIsValid(true);
    if (jobTypeInputRef.current.value.trim().length < 1) {
      setJobIsValid(false);
    }
    if (JobType) {
      const res = await fetch(
        "https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/types",
        {
          method: "POST",
          body: JSON.stringify({
            value: jobTypeInputRef.current.value,
            label: 132,
          }),
        }
      );
      console.log({
        value: jobTypeInputRef.current.value,
      });
      console.log(res);
      clearHandler();
    }
  };
  return (
    <Card title={"Job type"}>
      <Input
        ref={jobTypeInputRef}
        label="Job Type"
        input={{
          id: "jobType",
          type: "text",
          placeholder: "Enter your job type",
          invalid: `${!jobIsValid}`,
        }}
      />
      <div className={classes.buttons}>
        <Button onClick={clearHandler} type="button" clear>
          Clear
        </Button>
        <Button onClick={addJobTypeHandler} type="submit">
          Submit
        </Button>
      </div>
    </Card>
  );
};

export default JobType;
