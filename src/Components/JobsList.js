import React from "react";

import classes from "./JobsLists.module.css";
import deleteBtn from "../assets/delete.svg";

const JobLists = () => {
  return (
    <div className={classes.jobs}>
      <div className={classes.title}>Job type </div>
      <div className={classes.line}></div>
      <div className={classes.names}>
        <div className={classes.item}>№</div>
        <div className={classes.item}>Label</div>
        <div className={classes.item}></div>
      </div>
      <div className={classes.data}>
        <div className={classes.item}>1</div>
        <div className={classes.item}>Label</div>
        <div className={classes.item}>
          <img className={classes["v-open"]} src={deleteBtn} alt="open" />
        </div>
      </div>
    </div>
  );
};

export default JobLists;
