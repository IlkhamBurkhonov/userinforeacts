import { useEffect, useState } from "react";
import Card from "./UI/Card";
import Input from "./UI/Input";
import classes from "./WorkDetail.module.css";

const WorkDetail = ({
  companyNameRef,
  jobTypeRef,
  experienceRef,
  validity,
}) => {
  const [jobs, setJobs] = useState([]);

  useEffect(() => {
    const fetchJobs = async () => {
      const response = await fetch(
        "https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/types"
      );

      if (!response.ok) {
        throw new Error("Something went wrong!");
      }

      const responseData = await response.json();
      const arr = [];
      responseData.forEach((allJobs) => {
        arr.push(allJobs.value);
      });
      setJobs(arr);
    };

    fetchJobs().catch((error) => {
      console.log("ERROR: ", error);
    });
  }, []);

  return (
    <Card title={"Work Detail"}>
      <Input
        ref={companyNameRef}
        label="Company Name"
        input={{
          id: "companyName",
          type: "text",
          placeholder: "Enter your company name",
          invalid: `${!validity?.isCompanyValid}`,
        }}
      />
      <div className={classes.dropdown}>
        <Input
          ref={jobTypeRef}
          select
          options={jobs}
          label="Job Type"
          input={{
            id: "jobType",
            invalid: `${!validity?.isJobTypeValid}`,
          }}
        />
        <Input
          ref={experienceRef}
          select
          options={[1, 2, 3, 4, 5]}
          label="Experience"
          input={{
            id: "experience",
            invalid: `${!validity?.isExperienceValid}`,
          }}
        />
      </div>
    </Card>
  );
};

export default WorkDetail;
